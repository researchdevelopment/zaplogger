package store

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"time"
	"zaplogger/pkg/log"
	"zaplogger/pkg/pgx/log/factoryadapter"
)

type Store struct {
	pool *pgxpool.Pool
	log  *log.Factory
}

func NewStore(connStr string, logger *log.Factory, logLevel string) (*Store, error) {

	config, err := pgxpool.ParseConfig(connStr)
	if err != nil {
		return nil, err
	}

	config.ConnConfig.Logger = factoryadapter.NewLogger(logger)

	ll, err := pgx.LogLevelFromString(logLevel)
	if err != nil {
		ll = pgx.LogLevelError
	}
	config.ConnConfig.LogLevel = ll

	pool, err := pgxpool.ConnectConfig(context.Background(), config)
	if err != nil {
		return nil, err
	}
	return &Store{
		pool: pool,
		log:  logger,
	}, nil
}

func (s *Store) getPoolConnection(ctx context.Context) (*pgxpool.Conn, error) {
	return s.pool.Acquire(ctx)
}

func (s *Store) GetDatabaseVersion(ctx context.Context) (string, error) {

	conn, err := s.getPoolConnection(ctx)
	if err != nil {
		return "", err
	}
	defer conn.Release()

	var version string

	err = conn.QueryRow(ctx,
		"SELECT version()",
	).Scan(&version)
	return version, err
}

type User struct {
	UserID  int       `json:"user_id"`
	Created time.Time `json:"created"`
	Login   string    `json:"login"`
}

func (s *Store) GetById(ctx context.Context, id int, login string) (User, error) {

	span := trace.SpanFromContext(ctx)
	conn, _ := s.getPoolConnection(ctx)
	defer conn.Release()

	var user User
	err := conn.QueryRow(ctx,
		`SELECT user_id, created, login FROM users WHERE user_id = $1 AND login = $2;`,
		id,
		login,
	).Scan(&user.UserID, &user.Created, &user.Login)
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
	}

	return user, err
}

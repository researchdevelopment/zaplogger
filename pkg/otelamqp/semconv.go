package otelamqp

import "go.opentelemetry.io/otel/attribute"

const (
	MessagingPublishingContentType = attribute.Key("messaging.publishing.content_type")

	MessagingPublishingDeliveryMode = attribute.Key("messaging.publishing.delivery_mode")
)

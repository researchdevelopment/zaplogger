// Copyright The OpenTelemetry Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package otelamqp

import (
	"context"
	"github.com/streadway/amqp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"go.opentelemetry.io/otel/trace"
)

var tracer = otel.Tracer("amqp")

func StartProducerSpan(ctx context.Context, p amqp.Publishing, queue string) trace.Span {
	c := amqpHeadersCarrier(p.Headers)
	otel.GetTextMapPropagator().Extract(ctx, c)

	attrs := []attribute.KeyValue{
		semconv.MessagingSystemKey.String("rabbitmq"),
		semconv.MessagingProtocolKey.String("AMQP"),

		semconv.MessagingMessageIDKey.String(p.MessageId),
		MessagingPublishingContentType.String(p.ContentType),

		semconv.MessagingDestinationKey.String(""),           // Exchange
		semconv.MessagingDestinationKindKey.String("queue"),  // Exchange type
		semconv.MessagingRabbitmqRoutingKeyKey.String(queue), //queue

		semconv.MessagingMessagePayloadSizeBytesKey.Int(len(p.Body)),
	}
	opts := []trace.SpanStartOption{
		trace.WithAttributes(attrs...),
		trace.WithSpanKind(trace.SpanKindProducer),
	}

	ctx, span := tracer.Start(ctx, "amqp.producer", opts...)

	// TODO: inject
	otel.GetTextMapPropagator().Inject(ctx, c)

	return span
}

func EndProducerSpan(span trace.Span, err error) {
	if err != nil {
		span.SetStatus(codes.Error, err.Error())
	}
	span.End()
}

func StartConsumerSpan(ctx context.Context, d amqp.Delivery, queue string) (trace.Span, context.Context) {
	c := amqpHeadersCarrier(d.Headers)

	ctx = otel.GetTextMapPropagator().Extract(ctx, c)

	attrs := []attribute.KeyValue{
		semconv.MessagingSystemKey.String("rabbitmq"),
		semconv.MessagingProtocolKey.String("AMQP"),

		semconv.MessagingMessageIDKey.String(d.MessageId),
		MessagingPublishingContentType.String(d.ContentType),

		semconv.MessagingDestinationKey.String(""),           // Exchange
		semconv.MessagingDestinationKindKey.String("queue"),  // Exchange type
		semconv.MessagingRabbitmqRoutingKeyKey.String(queue), //queue

		semconv.MessagingMessagePayloadSizeBytesKey.Int(len(d.Body)),
	}

	opts := []trace.SpanStartOption{
		trace.WithAttributes(attrs...),
		trace.WithSpanKind(trace.SpanKindConsumer),
	}

	ctx, span := tracer.Start(ctx, "amqp.consumer", opts...)

	return span, ctx
}

func EndConsumerSpan(span trace.Span, err error) {
	if err != nil {
		span.RecordError(err)
	}
	defer span.End()
}

package bus

import (
	"github.com/google/uuid"
	"github.com/isayme/go-amqp-reconnect/rabbitmq"
	"github.com/streadway/amqp"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
	"time"
)

type Bus struct {
	url  string
	conn *rabbitmq.Connection

	log *zap.Logger
}

func NewBus(url string, log *zap.Logger) *Bus {
	return &Bus{url: url, log: log}
}

func (b *Bus) Connect() error {
	var err error
	b.conn, err = rabbitmq.Dial(b.url)
	return err
}

func (b *Bus) Connection() *rabbitmq.Connection {
	return b.conn
}

func (b *Bus) Publish(queue string, msg amqp.Publishing) {
	var (
		channel *rabbitmq.Channel
		err     error
	)
	for {
		for {
			channel, err = b.conn.Channel()
			if err == nil {
				break
			}
		}
		q, err := channel.QueueDeclare(
			queue,
			true,
			false,
			false,
			false,
			nil,
		)
		if err != nil {
			b.log.Error("failed to declare a queue, trying to resend", zap.Error(err))
			continue
		}
		err = channel.Publish(
			"",
			q.Name,
			false,
			false,
			msg)
		if err != nil {
			b.log.Error("failed to publish to queue, trying to resend", zap.Error(err))
			continue
		}
		channel.Close()
		break
	}
}

func ProtobufToAmqpMessage(pb interface{}) (amqp.Publishing, error) {
	body, err := proto.Marshal(pb.(proto.Message))
	if err != nil {
		return amqp.Publishing{}, err
	}
	return amqp.Publishing{
		MessageId:    uuid.New().String(),
		DeliveryMode: amqp.Persistent,
		ContentType:  "application/protobuf",
		Timestamp:    time.Now(),
		Body:         body,
	}, nil
}

func (b *Bus) DebugAmqpDeliveryMessage(tag string, m amqp.Delivery, r proto.Message) {
	b.log.Debug(tag,
		zap.String("cmp", "amqp"),
		zap.Reflect("MessageId", m.MessageId),
		zap.Reflect("RoutingKey", m.RoutingKey),
		zap.Reflect("Body", r),
	)
}

func (b *Bus) DebugAmqpPublishingMessage(tag string, m amqp.Publishing, r proto.Message) {
	b.log.Debug(tag,
		zap.String("cmp", "amqp"),
		zap.Reflect("MessageId", m.MessageId),
		zap.Reflect("Body", r),
	)
}

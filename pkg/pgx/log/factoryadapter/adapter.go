// Package zapadapter provides a logger that writes to a go.uber.org/zap.Logger.
package factoryadapter

import (
	"context"
	"zaplogger/pkg/log"

	"github.com/jackc/pgx/v4"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Logger struct {
	logger *log.Factory
}

func NewLogger(logger *log.Factory) *Logger {
	return &Logger{logger: logger}
}

func (pl *Logger) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	fields := make([]zapcore.Field, len(data))
	i := 0
	for k, v := range data {
		fields[i] = zap.Any(k, v)
		i++
	}

	switch level {
	case pgx.LogLevelTrace:
		pl.logger.For(ctx).Debug(msg, append(fields, zap.Stringer("PGX_LOG_LEVEL", level))...)
	case pgx.LogLevelDebug:
		pl.logger.For(ctx).Debug(msg, fields...)
	case pgx.LogLevelInfo:
		pl.logger.For(ctx).Info(msg, fields...)
	case pgx.LogLevelWarn:
		pl.logger.For(ctx).Warn(msg, fields...)
	case pgx.LogLevelError:
		pl.logger.For(ctx).Error(msg, fields...)
	default:
		pl.logger.For(ctx).Error(msg, append(fields, zap.Stringer("PGX_LOG_LEVEL", level))...)
	}
}

// Copyright (c) 2019 The Jaeger Authors.
// Copyright (c) 2017 Uber Technologies, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package log

import (
	"fmt"
	"github.com/hako/durafmt"
	"github.com/uptrace/opentelemetry-go-extra/otelutil"
	"go.opentelemetry.io/otel/attribute"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"go.opentelemetry.io/otel/trace"
	"math"
	"reflect"
	"strconv"
	"time"

	_ "github.com/hako/durafmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type spanLogger struct {
	logger     *zap.Logger
	span       trace.Span
	spanFields []zapcore.Field
}

func (sl spanLogger) Debug(msg string, fields ...zapcore.Field) {
	sl.logToSpan("debug", msg, fields...)
	sl.logger.Debug(msg, append(sl.spanFields, fields...)...)
}

func (sl spanLogger) Info(msg string, fields ...zapcore.Field) {
	sl.logToSpan("info", msg, fields...)
	sl.logger.Info(msg, append(sl.spanFields, fields...)...)
}

func (sl spanLogger) Warn(msg string, fields ...zapcore.Field) {
	sl.logToSpan("warn", msg, fields...)
	sl.logger.Warn(msg, append(sl.spanFields, fields...)...)
}

func (sl spanLogger) Error(msg string, fields ...zapcore.Field) {
	sl.logToSpan("error", msg, fields...)
	sl.logger.Error(msg, append(sl.spanFields, fields...)...)
}

func (sl spanLogger) DPanic(msg string, fields ...zapcore.Field) {
	sl.logToSpan("dpanic", msg, fields...)
	sl.logger.DPanic(msg, append(sl.spanFields, fields...)...)
}

func (sl spanLogger) Panic(msg string, fields ...zapcore.Field) {
	sl.logToSpan("panic", msg, fields...)
	sl.logger.DPanic(msg, append(sl.spanFields, fields...)...)
}

func (sl spanLogger) Fatal(msg string, fields ...zapcore.Field) {
	sl.logToSpan("fatal", msg, fields...)
	//tag.Error.Set(sl.span, true)
	sl.logger.Fatal(msg, append(sl.spanFields, fields...)...)
}

// With creates a child logger, and optionally adds some context fields to that logger.
func (sl spanLogger) With(fields ...zapcore.Field) Logger {
	return spanLogger{logger: sl.logger.With(fields...), span: sl.span, spanFields: sl.spanFields}
}

func (sl spanLogger) logToSpan(level string, msg string, fields ...zapcore.Field) {
	// TODO rather than always converting the fields, we could wrap them into a lazy logger
	//fa := fieldAdapter(make([]log.Field, 0, 2+len(fields)))
	//fa = append(fa, log.String("event", msg))
	//fa = append(fa, log.String("level", level))
	//for _, field := range fields {
	//	field.AddTo(&fa)
	//}
	//sl.span.LogFields(fa...)

	//keys := make([]attribute.KeyValue, 0, len(fields))
	//keys = append(keys, attribute.String("event", msg))
	//keys = append(keys, attribute.String("level", level))
	//for _, field := range fields {
	//		field.AddTo(&keys)
	//	}
	//fa := fieldAdapter(make([]attribute.KeyValue, 0, 1+len(fields)))
	//fa = append(fa, attribute.String("level", level))
	//for _, field := range fields {
	//	field.AddTo(&fa)
	//}
	//sl.span.AddEvent(msg, trace.WithAttributes(fa...))

	attrs := make([]attribute.KeyValue, 0, 1+len(fields))
	attrs = append(attrs, attribute.String("level", level))

	for _, f := range fields {
		if f.Type == zapcore.NamespaceType {
			// should this be a prefix?
			continue
		}
		attrs = appendField(attrs, f)
	}
	sl.span.AddEvent(msg, trace.WithAttributes(attrs...))

}

type fieldAdapter []attribute.KeyValue

//TODO: привевсти к виду оффициального расширения
// https://github.com/uptrace/opentelemetry-go-extra/blob/0fa5493e68adf84f5b45c513a257c95511117675/otelzap/otelzap.go?_pjax=%23js-repo-pjax-container%2C%20div%5Bitemtype%3D%22http%3A%2F%2Fschema.org%2FSoftwareSourceCode%22%5D%20main%2C%20%5Bdata-pjax-container%5D#L626

func (fa *fieldAdapter) AddBool(key string, value bool) {
	*fa = append(*fa, attribute.Bool(key, value))
}

func (fa *fieldAdapter) AddFloat64(key string, value float64) {
	*fa = append(*fa, attribute.Float64(key, value))
}

func (fa *fieldAdapter) AddFloat32(key string, value float32) {
	*fa = append(*fa, attribute.Float64(key, float64(value)))
}

func (fa *fieldAdapter) AddInt(key string, value int) {
	*fa = append(*fa, attribute.Int(key, value))
}

func (fa *fieldAdapter) AddInt64(key string, value int64) {
	*fa = append(*fa, attribute.Int64(key, value))
}

func (fa *fieldAdapter) AddInt32(key string, value int32) {
	*fa = append(*fa, attribute.Int64(key, int64(value)))
}

func (fa *fieldAdapter) AddInt16(key string, value int16) {
	*fa = append(*fa, attribute.Int64(key, int64(value)))
}

func (fa *fieldAdapter) AddInt8(key string, value int8) {
	*fa = append(*fa, attribute.Int64(key, int64(value)))
}

func (fa *fieldAdapter) AddUint(key string, value uint) {
	*fa = append(*fa, attribute.Float64(key, float64(value)))
}

func (fa *fieldAdapter) AddUint64(key string, value uint64) {
	*fa = append(*fa, attribute.Float64(key, float64(value)))
}

func (fa *fieldAdapter) AddUint32(key string, value uint32) {
	*fa = append(*fa, attribute.Float64(key, float64(value)))
}

func (fa *fieldAdapter) AddUint16(key string, value uint16) {
	*fa = append(*fa, attribute.Float64(key, float64(value)))
}

func (fa *fieldAdapter) AddUint8(key string, value uint8) {
	*fa = append(*fa, attribute.Float64(key, float64(value)))
}

func (fa *fieldAdapter) AddUintptr(key string, value uintptr) {}
func (fa *fieldAdapter) AddArray(key string, marshaler zapcore.ArrayMarshaler) error {
	*fa = append(*fa, otelutil.Attribute(key, marshaler))
	return nil
}
func (fa *fieldAdapter) AddComplex128(key string, value complex128)                {}
func (fa *fieldAdapter) AddComplex64(key string, value complex64)                  {}
func (fa *fieldAdapter) AddObject(key string, value zapcore.ObjectMarshaler) error { return nil }

func (fa *fieldAdapter) AddReflected(key string, value interface{}) error {
	*fa = append(*fa, otelutil.Attribute(key, value))
	return nil
}

func (fa *fieldAdapter) OpenNamespace(key string) {}

func (fa *fieldAdapter) AddDuration(key string, value time.Duration) {
	// TODO inefficient
	//*fa = append(*fa, log.String(key, value.String()))
}

func (fa *fieldAdapter) AddTime(key string, value time.Time) {
	// TODO inefficient
	//*fa = append(*fa, log.String(key, value.String()))
}

func (fa *fieldAdapter) AddBinary(key string, value []byte) {
	//*fa = append(*fa, log.Object(key, value))
}

func (fa *fieldAdapter) AddByteString(key string, value []byte) {
	//*fa = append(*fa, log.Object(key, value))
}

func (fa *fieldAdapter) AddString(key, value string) {
	if key != "" && value != "" {
		*fa = append(*fa, attribute.String(key, value))
	}
}

func appendField(attrs []attribute.KeyValue, f zapcore.Field) []attribute.KeyValue {
	switch f.Type {
	case zapcore.BoolType:
		attr := attribute.Bool(f.Key, f.Integer == 1)
		return append(attrs, attr)

	case zapcore.Int8Type, zapcore.Int16Type, zapcore.Int32Type, zapcore.Int64Type,
		zapcore.Uint32Type, zapcore.Uint8Type, zapcore.Uint16Type, zapcore.Uint64Type,
		zapcore.UintptrType:
		attr := attribute.Int64(f.Key, f.Integer)
		return append(attrs, attr)

	case zapcore.Float32Type, zapcore.Float64Type:
		attr := attribute.Float64(f.Key, math.Float64frombits(uint64(f.Integer)))
		return append(attrs, attr)

	case zapcore.Complex64Type:
		s := strconv.FormatComplex(complex128(f.Interface.(complex64)), 'E', -1, 64)
		attr := attribute.String(f.Key, s)
		return append(attrs, attr)
	case zapcore.Complex128Type:
		s := strconv.FormatComplex(f.Interface.(complex128), 'E', -1, 128)
		attr := attribute.String(f.Key, s)
		return append(attrs, attr)

	case zapcore.StringType:
		attr := attribute.String(f.Key, f.String)
		return append(attrs, attr)
	case zapcore.BinaryType, zapcore.ByteStringType:
		attr := attribute.String(f.Key, string(f.Interface.([]byte)))
		return append(attrs, attr)
	case zapcore.StringerType:
		attr := attribute.String(f.Key, f.Interface.(fmt.Stringer).String())
		return append(attrs, attr)

		//TODO:: format output short units
	case zapcore.DurationType, zapcore.TimeType:
		//attr := attribute.Int64(f.Key, f.Integer)
		duration := durafmt.Parse(time.Duration(f.Integer))
		attr := attribute.String(f.Key, duration.String())
		return append(attrs, attr)
	case zapcore.TimeFullType:
		attr := attribute.Int64(f.Key, f.Interface.(time.Time).UnixNano())
		return append(attrs, attr)
	case zapcore.ErrorType:
		err := f.Interface.(error)
		typ := reflect.TypeOf(err).String()
		attrs = append(attrs, semconv.ExceptionTypeKey.String(typ))
		attrs = append(attrs, semconv.ExceptionMessageKey.String(err.Error()))
		return attrs
	case zapcore.ReflectType:
		attr := otelutil.Attribute(f.Key, f.Interface)
		return append(attrs, attr)
	case zapcore.SkipType:
		return attrs

	case zapcore.ArrayMarshalerType, zapcore.ObjectMarshalerType:
		//return attrs
		attr := otelutil.Attribute(f.Key, f.Interface)
		return append(attrs, attr)

	default:
		attr := attribute.String(f.Key+"_error", fmt.Sprintf("otelzap: unknown field type: %v", f))
		return append(attrs, attr)
	}
}

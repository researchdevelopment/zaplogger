package log

import (
	"context"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap/zapcore"

	"go.uber.org/zap"
)

// Factory is the default logging wrapper that can create
// logger instances either for a given Context or context-less.
type Factory struct {
	logger *zap.Logger
}

// NewFactory creates a new Factory.
func NewFactory(logger *zap.Logger) Factory {
	return Factory{logger: logger}
}

// Bg creates a context-unaware logger.
func (b Factory) Bg() Logger {
	return logger(b)
}

//// For returns a context-aware Logger. If the context
//// contains an OpenTracing span, all logging calls are also
//// echo-ed into the span.
//func (b Factory) For(ctx context.Context) Logger {
//	span := trace.SpanFromContext(ctx)
//	if span != nil {
//		return b.With(
//			zap.String("tid", span.SpanContext().TraceID().String()),
//			zap.String("sid", span.SpanContext().SpanID().String()),
//		).Bg()
//	}
//	return b.Bg()
//}

// For returns a context-aware Logger. If the context
// contains an OpenTracing span, all logging calls are also
// echo-ed into the span.
func (b Factory) For(ctx context.Context) Logger {
	if span := trace.SpanFromContext(ctx); span != nil {
		logger := spanLogger{
			span:   span,
			logger: b.logger,
		}

		if span.SpanContext().TraceID().IsValid() {
			logger.spanFields = []zapcore.Field{
				zap.String("tid", span.SpanContext().TraceID().String()),
				zap.String("sid", span.SpanContext().SpanID().String()),
			}
		}

		return logger
	}
	return b.Bg()
}

func (b Factory) With(fields ...zap.Field) Factory {
	return Factory{
		logger: b.logger.With(fields...),
	}
}

func NewLogger(logLevel string, initFields map[string]interface{}) *zap.Logger {
	ll := zap.NewAtomicLevel()
	err := ll.UnmarshalText([]byte(logLevel))
	if err != nil {
		ll = zap.NewAtomicLevelAt(zap.DebugLevel)
	}

	config := zap.Config{
		Level:             ll,
		Development:       false,
		DisableCaller:     false,
		DisableStacktrace: false,
		Sampling:          nil,
		Encoding:          "json",
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:     "msg",
			LevelKey:       "lvl",
			TimeKey:        "tm",
			NameKey:        "lgr",
			CallerKey:      "clr",
			StacktraceKey:  "stack",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.RFC3339TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
			EncodeName:     zapcore.FullNameEncoder,
		},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
		InitialFields:    initFields,
	}

	opts := []zap.Option{
		zap.AddCallerSkip(1), // traverse call depth for more useful log lines
		zap.AddCaller(),
	}

	logger, _ := config.Build(opts...)
	return logger
}

package main

import (
	"context"
	"flag"
	"fmt"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/streadway/amqp"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
	"zaplogger/pkg/bus"
	"zaplogger/pkg/otelamqp"
	"zaplogger/pkg/pb"
	"zaplogger/pkg/tr"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"google.golang.org/grpc"

	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"go.opentelemetry.io/otel/trace"

	"github.com/go-chi/chi/v5"
)

var (
	listenAddr string
	logger     *zap.Logger
	rabbit     *bus.Bus
)

const (
	SERVICE = "api"
	VERSION = "0.0.1-e8e6ec"

	BRANCH = "develop"
	COMMIT = "e8e6ecef69d444636ae41d6d1e7fc7c1b3f3b07d"

	ENVIRONMENT = "dev"
)

func main() {

	flag.StringVar(&listenAddr, "listen", ":8080", "server listen address")
	flag.Parse()

	logger = NewLogger()
	defer PanicHandler(logger)

	// logger.Info("Test info")
	// logger.Warn("Test warn")
	// logger.Error("Test error")

	//TestFunc(logger)

	shutdown, _ := tr.InitProvider(SERVICE, VERSION, ENVIRONMENT)
	defer shutdown()

	logger.Info("Done!")

	r := chi.NewRouter()

	r.Get("/", indexHandler)

	r.Route("/v1", func(v1 chi.Router) {
		v1.Use(tr.Tracer)
		v1.Get("/span", spanHandler)
		v1.Get("/sum", sumHandler)
		v1.Get("/rabbit", rabbitHandler)
	})

	server := &http.Server{
		Addr:    listenAddr,
		Handler: r,
		// ErrorLog:     logger,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  15 * time.Second,
	}

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		logger.Info("Server is shutting down...")

		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()

		server.SetKeepAlivesEnabled(false)
		if err := server.Shutdown(ctx); err != nil {
			logger.Error("Could not gracefully shutdown the server", zap.Error(err))
		}
		close(done)
	}()

	rabbit = bus.NewBus(bus.AMQP_CONNECTION, logger)
	if err := rabbit.Connect(); err != nil {
		logger.Error("Could not connect Rabbit",
			zap.String("connection", bus.AMQP_CONNECTION),
			zap.Error(err),
		)
	}

	logger.Info("Server is ready to handle requests at", zap.String("detail", listenAddr))
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logger.Error("Could not listen", zap.Error(err))
	}

	<-done
	logger.Info("Server stopped")

}

func TestFunc(logger *zap.Logger) {
	logger.Error("Test msg TestFunc")
	// panic("testpanic")
}

func NewLogger() *zap.Logger {
	config := zap.Config{
		Level:             zap.NewAtomicLevelAt(zap.InfoLevel),
		Development:       false,
		DisableCaller:     false,
		DisableStacktrace: false,
		Sampling:          nil,
		Encoding:          "json",
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:     "msg",
			LevelKey:       "level",
			TimeKey:        "time",
			NameKey:        "logger",
			CallerKey:      "file",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.RFC3339TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
			EncodeName:     zapcore.FullNameEncoder,
		},
		OutputPaths:      []string{"stdout"},
		ErrorOutputPaths: []string{"stderr"},
		InitialFields: map[string]interface{}{
			"app": SERVICE,
			"env": ENVIRONMENT,
			"ver": VERSION,
		},
	}

	opts := []zap.Option{
		zap.AddCallerSkip(1), // traverse call depth for more useful log lines
		zap.AddCaller(),
	}

	logger, _ = config.Build(opts...)
	return logger
}

func PanicHandler(logger *zap.Logger) {
	if r := recover(); r != nil {
		logger.DPanic("caught panic", zap.Any("detail", r))
		// panic(r)
	}
}

func indexHandler(w http.ResponseWriter, req *http.Request) {

	// 	cxt := req.Context()
	// 	_ = ctx

	logger.Info("index")

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("app: %s ver: %s", SERVICE, VERSION)))
}

func spanHandler(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	span := trace.SpanFromContext(ctx)

	doSomeAction(req.Context(), "12345")

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("SpanID=%s TraceID=%s",
		span.SpanContext().SpanID().String(),
		span.SpanContext().TraceID().String(),
	)))
}

func doSomeAction(ctx context.Context, v string) {
	// ctx, span := otel.Tracer(service).Start(ctx, "doSomeAction")
	ctx, span := tr.StartSpan(ctx)
	defer span.End()
	span.AddEvent("I am a child span!")

	time.Sleep(10 * time.Millisecond)

	dataRequest(ctx)

}

func dataRequest(ctx context.Context) {
	// ctx, span := otel.Tracer("database").Start(ctx, "dataRequest")
	_, span := tr.StartSpan(ctx)
	defer span.End()
	span.SetAttributes(semconv.DBSystemPostgreSQL)
	span.SetAttributes(attribute.Key("sql").String("SELECT * FROM users"))

	span.AddEvent("with param", trace.WithAttributes(attribute.Int("pid", 4328), attribute.String("signal", "SIGHUP")))

	time.Sleep(20 * time.Millisecond)
	span.SetStatus(codes.Error, "database error")
}

func panicHandler(w http.ResponseWriter, req *http.Request) {}

func sumHandler(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	span := trace.SpanFromContext(ctx)

	conn, _ := grpc.Dial("127.0.0.1:50051",
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(grpc_middleware.ChainUnaryClient(
			//grpc_zap.UnaryClientInterceptor(zapLogger),
			otelgrpc.UnaryClientInterceptor(),
		)),
	)

	client := pb.NewAddsvcClient(conn)

	resp, err := client.Sum(ctx, &pb.SumRequest{A: 1, B: 2})

	if err != nil {
		log.Fatalf("could not get answer: %v", err)
	}

	logger.Info("response",
		zap.Int64("a", 1),
		zap.Int64("b", 2),
		zap.Int64("rs", resp.GetRs()),
		zap.String("sid", span.SpanContext().SpanID().String()),
		zap.String("tid", span.SpanContext().TraceID().String()),
	)
	//}(span)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("GRPC SpanID=%s TraceID=%s Result=%d",
		span.SpanContext().SpanID().String(),
		span.SpanContext().TraceID().String(),
		resp.GetRs(),
	)))
}

func rabbitHandler(w http.ResponseWriter, req *http.Request) {
	//Make a connection
	conn, err := amqp.Dial(bus.AMQP_CONNECTION)
	if err != nil {
		logger.Error("Failed to connect to RabbitMQ", zap.Error(err))
		return
	}

	defer conn.Close()

	//Ccreate a channel
	ch, _ := conn.Channel()
	defer ch.Close()

	//if err := ch.ExchangeDeclare(
	//	"testtrace",         // name
	//	amqp.ExchangeDirect, // type
	//	true,                // durable
	//	false,               // auto-deleted
	//	false,               // internal
	//	false,               // noWait
	//	nil,                 // arguments
	//); err != nil {
	//	logger.Error("Failed Exchange Declare", zap.Error(err))
	//	return
	//}

	queue := "hello"
	//Declare a queue
	q, _ := ch.QueueDeclare(
		queue, // name of the queue
		false, // should the message be persistent? also queue will survive if the cluster gets reset
		false, // autodelete if there's no consumers (like queues that have anonymous names, often used with fanout exchange)
		false, // exclusive means I should get an error if any other consumer subsribes to this queue
		false, // no-wait means I don't want RabbitMQ to wait if there's a queue successfully setup
		nil,   // arguments for more advanced configuration
	)

	//Publish a message
	body := queue
	publishing := amqp.Publishing{
		ContentType:  "text/plain",
		Body:         []byte(body),
		DeliveryMode: amqp.Transient, // 1=non-persistent, 2=persistent
		Priority:     0,              // 0-9
		Headers:      amqp.Table{},
	}
	span := otelamqp.StartProducerSpan(req.Context(), publishing, q.Name)
	defer otelamqp.EndProducerSpan(span, err)

	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		publishing,
	)

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Rabbit"))
}

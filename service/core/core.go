package main

import (
	"context"
	"flag"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/streadway/amqp"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	semconv "go.opentelemetry.io/otel/semconv/v1.7.0"
	"go.opentelemetry.io/otel/trace"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"

	"net"
	"os"
	"os/signal"
	"time"
	"zaplogger/internal/store"
	"zaplogger/pkg/bus"
	"zaplogger/pkg/log"
	"zaplogger/pkg/otelamqp"
	"zaplogger/pkg/pb"
	"zaplogger/pkg/tr"

	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.uber.org/zap"
)

var (
	listenAddr string
	//logger     *zap.Logger
)

const (
	SERVICE = "core"
	VERSION = "0.0.1-e8e6ec"

	BRANCH = "develop"
	COMMIT = "e8e6ecef69d444636ae41d6d1e7fc7c1b3f3b07d"

	ENVIRONMENT = "dev"
)

type GRPCServer struct {
	logger *log.Factory
	db     *store.Store
}

func (s *GRPCServer) Sum(ctx context.Context, in *pb.SumRequest) (resp *pb.SumReply, err error) {
	ctx, span := tr.StartSpan(ctx)
	defer func() {
		//span.SetAttributes(attribute.Int64("a", in.GetA()))
		//span.SetAttributes(attribute.Int64("b", in.GetB()))
		//span.SetAttributes(attribute.Int64("rs", resp.GetRs()))
		//if err != nil {
		//	span.SetAttributes(attribute.String("error", err.Error()))
		//}

		s.logger.For(ctx).Info("sum response",
			zap.Int64("a", in.GetA()),
			zap.Int64("b", in.GetB()),
			zap.Int64("rs", resp.GetRs()),
		)
		span.End()
	}()

	doSomeActionDb(ctx, s.db, s.logger)

	return &pb.SumReply{
		Rs: in.A + in.B,
	}, nil
}

func (s *GRPCServer) Concat(ctx context.Context, in *pb.ConcatRequest) (*pb.ConcatReply, error) {

	return &pb.ConcatReply{
		Rs: in.A + in.B,
	}, nil
}

func main() {
	var (
		grpcPort = flag.String("grpc.port", "50051", "GRPC server port")
	)
	flag.Parse()

	var logger log.Factory
	{
		logger = log.NewFactory(
			log.NewLogger("develop",
				map[string]interface{}{
					"svc": SERVICE,
					"ver": VERSION,
					"env": ENVIRONMENT,
				}),
		)
	}
	defer PanicHandler(&logger)

	logger.Bg().Info("Starting service",
		zap.String("branch", BRANCH),
		zap.String("commit", COMMIT),
	)

	shutdown, err := tr.InitProvider(SERVICE, VERSION, ENVIRONMENT)
	if err != nil {
		logger.Bg().Fatal(
			"failed init trace provider",
			zap.Error(err),
		)
	}

	defer shutdown()

	//"postgres://username:password@localhost:5432/database_name"
	dbConnectionString := "postgres://test:test@dcr:5432/test"
	db, err := store.NewStore(dbConnectionString, &logger, "debug")
	if err != nil {
		logger.Bg().Fatal("not connect to database",
			zap.Error(err),
		)
	}
	_ = db
	//logger.Bg().Info("successfully connected to database")
	//ver, _ := db.GetDatabaseVersion(context.Background())
	//logger.Bg().Info("database version",
	//	zap.String("postgres_version", ver),
	//)

	//opts := grpc.ServerOption(grpc.WithInsecure())

	server := grpc.NewServer(
		grpc.KeepaliveParams(keepalive.ServerParameters{MaxConnectionAge: 1 * time.Minute}),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			//grpc_ctxtags.UnaryServerInterceptor(),
			//grpc_zap.UnaryServerInterceptor(log.NewLogger("debug", map[string]interface{}{})),
			otelgrpc.UnaryServerInterceptor(),
		)),
	)
	instance := &GRPCServer{
		logger: &logger,
		db:     db,
	}

	pb.RegisterAddsvcServer(server, instance)

	listener, err := net.Listen("tcp", ":"+*grpcPort)
	if err != nil {
		logger.Bg().Fatal(
			"Unable to create grpc listener",
			zap.Error(err),
		)
	}

	logger.Bg().Info("Done!")

	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	go func() {
		<-quit
		logger.Bg().Info("Server is shutting down...")

		server.GracefulStop()
		close(done)
	}()

	go rabbitConsume(context.Background(), &logger)

	logger.Bg().Info(
		"Server is ready to handle requests at",
		zap.String("detail", listenAddr),
	)
	if err := server.Serve(listener); err != nil {
		logger.Bg().Error(
			"Could not listen",
			zap.Error(err),
		)
	}

	<-done
	logger.Bg().Info("Server stopped")

}

func PanicHandler(logger *log.Factory) {
	if r := recover(); r != nil {
		logger.Bg().DPanic("caught panic",
			zap.Any("detail", r),
		)
		// panic(r)
	}
}

func rabbitConsume(ctx context.Context, logger *log.Factory) {
	//Make a connection
	conn, err := amqp.Dial(bus.AMQP_CONNECTION)
	if err != nil {
		logger.For(ctx).Error(
			"Failed to connect to RabbitMQ",
			zap.Error(err),
		)
		return
	}
	defer conn.Close()

	//Create a channel
	ch, _ := conn.Channel()
	defer ch.Close()

	//if err := ch.ExchangeDeclare(
	//	"testtrace",         // name
	//	amqp.ExchangeDirect, // type
	//	true,                // durable
	//	false,               // auto-deleted
	//	false,               // internal
	//	false,               // noWait
	//	nil,                 // arguments
	//); err != nil {
	//	logger.Error("Failed Exchange Declare", zap.Error(err))
	//	return
	//}

	queue := "hello"
	deliveries, err := ch.Consume(
		queue, // name
		"",    // consumerTag,
		false, // noAck
		false, // exclusive
		false, // noLocal
		false, // noWait
		nil,   // arguments
	)
	if err != nil {
		logger.For(ctx).Error(
			"Failed Queue Consume",
			zap.Error(err),
		)
		return
	}

	for d := range deliveries {
		logger.For(ctx).Info("New message")
		//logger.For(ctx).Info("message span context",
		//	zap.Reflect("headers", d.Headers),
		//)
		span, ctx := otelamqp.StartConsumerSpan(ctx, d, queue)
		//logger.Info("extract trace_id",
		//	zap.String("trace_id", span.SpanContext().TraceID().String()),
		//)
		time.Sleep(10 * time.Millisecond)
		doSomeAction(ctx, logger, "param value")
		time.Sleep(10 * time.Millisecond)
		err := d.Ack(false)
		logger.For(ctx).Info("Ask message")

		otelamqp.EndProducerSpan(span, err)
	}
	logger.For(ctx).Info("stop consume")

}

func doSomeAction(ctx context.Context, logger *log.Factory, v string) {
	ctx, span := tr.StartSpan(ctx)
	defer span.End()
	logger.For(ctx).Info("I am a child span!",
		zap.String("param", v),
	)

	time.Sleep(10 * time.Millisecond)

	dataRequest(ctx, logger)

}

func dataRequest(ctx context.Context, logger *log.Factory) {
	// ctx, span := otel.Tracer("database").Start(ctx, "dataRequest")
	_, span := tr.StartSpan(ctx)
	defer span.End()
	span.SetAttributes(semconv.DBSystemPostgreSQL)
	span.SetAttributes(attribute.Key("sql").String("SELECT * FROM users"))

	span.AddEvent("with param", trace.WithAttributes(attribute.Int("pid", 4328), attribute.String("signal", "SIGHUP")))

	time.Sleep(20 * time.Millisecond)
	span.SetStatus(codes.Error, "database error")
}

func doSomeActionDb(ctx context.Context, db *store.Store, logger *log.Factory) (*store.User, error) {
	ctx, span := tr.StartSpan(ctx)
	defer span.End()

	//ver, _ := db.GetDatabaseVersion(ctx)
	//logger.For(ctx).Info("database version",
	//	zap.String("postgres_version", ver),
	//)

	u, err := db.GetById(ctx, 1, "admin")
	if err != nil {
		return nil, err
	}
	logger.For(ctx).Info("success get user by id",
		zap.Reflect("user", u),
	)
	return &u, err
}
